
import java.util.*;
public class Controlador {

	Controlador(){
		
		Automovil a = new Automovil();
		
		/* valor por defecto */
		String cilindrada = "1.2";
		
		/* dos formas distintas de instanciar */
		//Motor m = new Motor(cilindrada);
		Motor m1 = new Motor();
		
		a.setMotor(m1);
		
		Velocimetro v = new Velocimetro();
		a.setVelocimetro(v);
		
		Estanque e = new Estanque();
		a.setEstanque(e);
		
		for (int i=0; i<4;i++) {
			a.setRueda(new Rueda());
		}
		
		for (double i=0.0; i< a.getEstanque().getVolumen(); ) {
			a.reporte_automovil();
			try {
				
				System.out.println("Para el auto encendido 0 y para apagar cualquier otro número");
				Scanner entrada = new Scanner(System.in);
				int boton = entrada.nextInt();
				if (boton == 0) {
					System.out.println("Indique la velocidad(km/h) en modo double a la que se recorrerá (máximo 120 km/h)");
					Scanner entrada1 = new Scanner(System.in);
					double veloz = entrada1.nextDouble();
					if (veloz>120) {
						veloz=120;
					}
					a.getVelocimetro().setVelocidad(veloz);
				}
				if (boton == 0) {
					if (a.getEncendido()!=true) {
						a.enciende_automovil();
						a.getEstanque().setresta(0.1);	
					}
					a.movimiento();
					a.gasto_litros();
					a.gasto_ruedas();
				}else{
					a.apaga_automovil();
				}
			}
			catch (Exception error) {
			}
		}
	}
}
