import java.util.*;

public class Automovil {

	private Motor motor;
	private Velocimetro velocimetro;
	private ArrayList<Rueda> rueda;
	private Estanque estanque;
	private Boolean encendido ;
	double distancia;
	double corrido;
	double Ttotal;
	private float Tiempo;
	private int cambio_ruedas;
	

	Automovil(){
		this.rueda = new ArrayList<Rueda>();
		this.encendido = false;
	}
	
	public Motor getMotor() {
		return motor;
	}
	
	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	public Velocimetro getVelocimetro() {
		return velocimetro;
	}

	public void setVelocimetro(Velocimetro velocimetro) {
		this.velocimetro = velocimetro;
	}

	public Estanque getEstanque() {
		return estanque;
	}

	public void setEstanque(Estanque estanque) {
		this.estanque = estanque;
	}

	public Boolean getEncendido() {
		return encendido;
	}

	public void setEncendido(Boolean encendido) {
		this.encendido = encendido;
	}

	public ArrayList<Rueda> getRueda() {
		return rueda;
	}

	public void setRueda(Rueda rueda) {
		this.rueda.add(rueda);
	}
	
	public void enciende_automovil() {
		
		this.encendido = true;
		
	}
	
	public void apaga_automovil() {
		this.velocimetro.setVelocidad(0.0);
		this.encendido = false;
	}
	
	public void movimiento() {
		Random Naleatorio = new Random();
		float tiempo = Naleatorio.nextFloat()*9+1;
	    this.Tiempo = tiempo; 
		this.Ttotal = Ttotal + tiempo;
		this.corrido = this.velocimetro.getVelocidad()*(tiempo/3600);
		this.distancia = distancia + corrido;
	}
	
	public void chequeo_ruedas() {
		int i = 0;
		for (Rueda r : this.rueda) {
			i = i + 1;
			if (10 >= r.getDesgaste()) {
				r.recambio();
				System.out.println("La rueda " + i + " ha sido cambiada");
				this.cambio_ruedas = this.cambio_ruedas + 1;
			}
			System.out.println("El estado de la rueda "+ i + " es "  + r.getDesgaste());
		}
	}
	public void cambio_rueda() {
		
	}
	
	public void gasto_litros() {
		
		if (this.motor.getCilindrada()=="1,6") {
			this.estanque.setresta(this.corrido/14);
		}
		else {
			this.estanque.setresta(this.corrido/20);
		}
	}
	
	public void gasto_ruedas() {
		for (Rueda r : this.rueda) {
			Random porcentaje = new Random();
			int Pdesgaste = porcentaje.nextInt(10)+1;
			r.setDesgaste(Pdesgaste);
		}
	}
	
	public void reporte_automovil() {
		System.out.println("El automovil tiene un motor de " + this.motor.getCilindrada());
		System.out.println("El estado actual del estanque es: " + this.estanque.getVolumen());
		chequeo_ruedas();
		System.out.println("La velocidad actual del automovil es " + this.velocimetro.getVelocidad() + " km/h");
		System.out.println("El auto esta encendido: " + this.getEncendido());
		if (this.getEncendido()) {
			System.out.println("El auto se mueve " + corrido + " km en un tiempo de " + Tiempo + " segundos");
		}
		else {
			System.out.println("El auto no se mueve en un tiempo de " + Tiempo);
		}
		System.out.println("El auto ha recorrido " + distancia + " en un tiempo total de " + Ttotal + " segundos");
		System.out.println("Se han cambiado " + this.cambio_ruedas + " ruedas");
	}
	
}
