
public class Velocimetro {
	
	/* Velocidad refiere a velocidad actual del automovil */
	private double velocidad;
	private double velocidad_maxima = 120;
	
	
	public double getVelocidad() {
		return velocidad;
	}
	
	public void setVelocidad(double velocidad) {
		this.velocidad = velocidad;
	}
	
	public double getVelocidadMaxima() {
		return this.velocidad_maxima;
	}
	

}
