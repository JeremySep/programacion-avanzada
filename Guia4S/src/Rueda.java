
public class Rueda {
	
	private int desgaste = 100;

	public int getDesgaste() {
		return desgaste;
	}

	public void setDesgaste(int desgaste) {
		this.desgaste = this.desgaste - (this.desgaste*desgaste/100);
	}
	
	public void recambio() {
		this.desgaste = 100;
	}

}
