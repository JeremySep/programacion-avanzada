
public class Estanque {

	private Double volumen = 32.0;
	private Double resta = 0.0;
	
	public Double getVolumen() {
		return volumen;
	}

	public void setVolumen(Double volumen) {
		this.volumen = volumen;
	}
	
	public void setresta(Double resta) {
		if (this.volumen <= 0) {
			System.exit(0);
		}
		this.resta = resta;
		this.volumen = this.volumen - (this.volumen*this.resta);
	}
}
