
public class Controlador {

	Controlador(){
		
		Automovil a = new Automovil();
			
		/* dos formas distintas de instanciar */
		//Motor m = new Motor(cilindrada);
		Motor m1 = new Motor();
		
		a.setMotor(m1);
		
		Velocimetro v = new Velocimetro();
		a.setVelocimetro(v);
		
		Estanque e = new Estanque();
		a.setEstanque(e);
		
		for (int i=0; i<4;i++) {
			a.setRueda(new Rueda());
		}
		
		//a.chequeo_ruedas();
		a.reporte_automovil();
		
	}
}
