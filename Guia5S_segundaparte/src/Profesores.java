
public class Profesores extends Empleados {
	
	private String departamento;
	
	Profesores(){
		
		super();
		
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	
	public String muestradetalles() {
		return "Profesor@ datos: nombre: " + this.getNombre() + " Apellidos: " + this.getApellidos() + " Numero de identificacion: " +  this.getId() + " año de incorporacion: " + this.getAñoIncorporacion() + " departamento: " + this.getDepartamento();
	}

}
