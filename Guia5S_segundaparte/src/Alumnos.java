
public class Alumnos extends Persona {
	
	private String curso;
	
	Alumnos(){
		super();
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}
	
	public String muestradetalles() {
		return "Alumn@ datos: ombre: " + this.getNombre() + " Apellidos: " + this.getApellidos() + " Numero de identificacion: " +  this.getId() + " curso: " + this.getCurso();
	}
}
