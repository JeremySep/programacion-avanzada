
public class Administrativo extends Empleados {
	
	private String seccion;
	
	Administrativo(){
		
		super();
		
	}

	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	
	public String muestradetalles() {
		return "Asministrativ@ datos: nombre: " + this.getNombre() + " Apellidos: " + this.getApellidos() + " Numero de identificacion: " + this.getId() + " año de incorporacion: " + this.getAñoIncorporacion() + " seccion:" + this.getSeccion();
	}
}
