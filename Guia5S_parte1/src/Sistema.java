import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Sistema {

	Sistema(){
		
		ObjetoDeInventario a = new ObjetoDeInventario(); 
		
		ArrayList<Revistas> ListaRevistas= new ArrayList<Revistas>();
		
		ArrayList<Libros> ListaLibros= new ArrayList<Libros>();
		
		int respuesta = 1;
		
		for(int i=1; i== respuesta;) {
			
			System.out.println("Escriba 1 si desea ingresar un objeto al catalogo"
					+ " o 2 si desea ver la lista del catalogo");
			Scanner seleccion = new Scanner(System.in);
		
		
			if(seleccion.nextInt()==1) {
			
				System.out.println("Introduzca 1 si el objeto es de tipo libro y 2 si es de tipo revista:");
				Scanner introtipo = new Scanner(System.in);
				int Itipo = introtipo.nextInt();
			
				System.out.println("Introduzca el codigo de objeto a almacenar:");
				Scanner idcodigo = new Scanner(System.in);
				String Icodigo = idcodigo.nextLine();
				
				System.out.println("Introduzca el titulo del objeto a almacenar:");
				Scanner introtitulo = new Scanner(System.in);
				String Ititulo = introtitulo.nextLine();
			
				System.out.println("Introduzca el año de emicion del objeto a almacenar");
				Scanner introaño = new Scanner(System.in);
				int Iaño = introaño.nextInt();
			
				if(Itipo==1) {
					Libros l = new Libros();
					ListaLibros.add(l);
					int j = 0;
					for(Libros n: ListaLibros) {
						if(Icodigo.equals(ListaLibros.get(j).getCodigo())) {
							System .out.println("Se ha reemplazado el libro: " + ListaLibros.get(j).getTitulo());
							ListaLibros.remove(j);
						}
						j++;
					}
					
					l.setCodigo(Icodigo);
					l.setTitulo(Ititulo);
					l.setAño_publicacion(Iaño);
					l.muestra_detalle();
				}
				else {
					System.out.println("Ingrese el genero de la revista:");
					Scanner introgenero = new Scanner(System.in);
					String Igenero = introgenero.nextLine();
					Igenero = Igenero.toUpperCase();
					System.out.println("Ingrese el número de edición:");
					Scanner introedicion = new Scanner(System.in);
					int Iedicion = introedicion.nextInt();
					Revistas r = new Revistas(Iedicion);
					ListaRevistas.add(r);
					int j = 0;
					for(Revistas n: ListaRevistas) {
						if(Icodigo.equals(ListaRevistas.get(j).getCodigo())) {
							System .out.println("Se ha reemplazado el libro: " + ListaRevistas.get(j).getTitulo());
							ListaRevistas.remove(j);
						}
						j++;
					}
					r.setCodigo(Icodigo);
					r.setTitulo(Ititulo);
					r.setAño_publicacion(Iaño);
					r.setGenero(Igenero);
					r.muestra_detalle();
				}
				
				
			}
		
			else {
				int e=0;
				Cuenta c = new Cuenta();
				System.out.println("Libros: ");
				for(ObjetoDeInventario y: ListaLibros) {
					ListaLibros.get(e).muestra_detalle();
					if("Ocupado".equals(ListaLibros.get(e).getEstado()) && ((int)ListaLibros.get(e).getHoraDePrestado().getHour()/24)-5 >=0) {
						c.setSaldo(ListaLibros.get(e).DiferenciaDeCuenta());
					}
					e++;
				}
				e=0;
				System.out.println("Revistas: ");
				for(ObjetoDeInventario y: ListaRevistas) {
					ListaRevistas.get(e).muestra_detalle();
					e++;
				}
				System.out.println("Escoja alguna accion: 1=pedir, 2=devolver y 3=consulta saldo");
				Scanner introaccion = new Scanner(System.in);
				int Iaccion = introaccion.nextInt();
				System.out.println("Indique el tipo de objeto al que desea realizar la accion: 1=Libro y 2=Revista");
				Scanner introObjetoFoco = new Scanner(System.in);
				int IObjetoFoco = introObjetoFoco.nextInt();
				System.out.println("Escriba el codigo del producto que desee realizar la accion: ");
				Scanner introObjetoCodigo = new Scanner(System.in);
				String IObjetoCodigo = introObjetoCodigo.nextLine();
				
				if(Iaccion == 1) {
					if(IObjetoFoco == 1) {
						int j = 0;
						for(Libros l: ListaLibros) {
							if(ListaLibros.get(j).getCodigo().equals(IObjetoCodigo)) {
								ListaLibros.get(j).setEstado("Ocupado");
								ListaLibros.get(j).setHoraDePrestado(LocalDateTime.now());
							}
						}
					}
					else {
						int j = 0;
						for(Revistas l: ListaRevistas) {
							if(ListaRevistas.get(j).getCodigo().equals(IObjetoCodigo)) {
								ListaRevistas.get(j).setEstado("Ocupado");
								ListaRevistas.get(j).setHoraDePrestado(LocalDateTime.now());
							}
						}
					}
				}
				if(Iaccion == 2) {
					if(IObjetoFoco == 1) {
						int j = 0;
						for(Libros l: ListaLibros) {
							if(ListaLibros.get(j).getCodigo().equals(IObjetoCodigo)) {
								ListaLibros.get(j).setEstado("Disponible");
								ListaLibros.get(j).setHoraDevuelto(LocalDateTime.now());
							}
						}
					}
					else {
						int j = 0;
						for(Revistas l: ListaRevistas) {
							if(ListaRevistas.get(j).getCodigo().equals(IObjetoCodigo)) {
								ListaLibros.get(j).setEstado("Disponible");
								ListaRevistas.get(j).setHoraDevuelto(LocalDateTime.now());
							}
						}
					}
				}
				else { 
					System.out.println("Su saldo a pagar es de: " + c.getSaldo() + " pesos");
				}
			}		
			
			System.out.println("¿Desea terminar el programa? SI=0/NO=1");
			Scanner Irespuesta = new Scanner(System.in);
			respuesta = Irespuesta.nextInt();
			
		}
	}
}
