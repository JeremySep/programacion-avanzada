import java.util.*;
import java.time.*;

public class ObjetoDeInventario {
	
	private String codigo;
	
	private String titulo;
	
	private int año_publicacion;
	
	public ObjetoDeInventario (){
		
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getAño_publicacion() {
		return año_publicacion;
	}

	public void setAño_publicacion(int año_publicacion) {
		this.año_publicacion = año_publicacion;
	}	
}
