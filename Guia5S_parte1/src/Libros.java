import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.time.*;
public class Libros extends ObjetoDeInventario implements Controlador{
	
	private String estado = "Disponible";
	
	private LocalDateTime HoraDePrestado;
	
	private LocalDateTime HoraDevuelto;
	
	private int Diferencia;
	
	Libros(){
		super();
	
	}
	
	public void muestra_detalle() {
			System.out.println("Titulo: " + this.getTitulo() + " codigo: " + this.getCodigo() + " año de publicacion: "
					+ this.getAño_publicacion() + " estado: " + this.estado);
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public String getEstado() {
		return this.estado;
	}
		
	public void setHoraDePrestado(LocalDateTime localDateTime) {
		this.HoraDePrestado = localDateTime;
	}
	
	public LocalDateTime getHoraDePrestado() {
		return HoraDePrestado;
	}
	
	public int DiferenciaDeCuenta() {
		
		if((int)ChronoUnit.HOURS.between(HoraDePrestado,HoraDevuelto)/24<=5) {
			this.Diferencia = Diferencia - (1290 * (((int)((int)ChronoUnit.HOURS.between(HoraDePrestado,HoraDevuelto)/24))-5));
		}
		return this.Diferencia;
	}

	public LocalDateTime getHoraDevuelto() {
		return HoraDevuelto;
	}

	public void setHoraDevuelto(LocalDateTime localDateTime) {
		this.HoraDevuelto = localDateTime;
	}

}
