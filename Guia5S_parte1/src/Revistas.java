import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.time.*;
public class Revistas extends ObjetoDeInventario implements Controlador{
	
	private int edicion;
	
	private String estado = "Disponible";
	
	private String genero;
	
	private LocalDateTime HoraDePrestado;
	
	private LocalDateTime HoraDevuelto;
		
	Revistas(int Iedicion){
		super();
		this.edicion = Iedicion;
	}
	
	public void muestra_detalle() {
		System.out.println("Titulo: " + this.getTitulo() + " codigo: " + this.getCodigo() + " año de publicacion: "
				+ this.getAño_publicacion() + " edición: " + this.edicion + " estado: " + this.estado + " genero: " + this.genero);
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public String getEstado() {
		return estado;
	}
	
	public int getEdicion() {
		return this.edicion;
	}
		
	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	public LocalDateTime getHoraDePrestado() {
		return HoraDePrestado;
	}
	
	public void setHoraDePrestado(LocalDateTime localDateTime) {
		this.HoraDePrestado = localDateTime;
	}
	
	public void Diferencia() {
		if((int)ChronoUnit.HOURS.between(HoraDePrestado,HoraDevuelto)/24<=1 && this.genero.equals("COMIC")) {
			this.estado = "Perdida";
		}
		else if((int)ChronoUnit.HOURS.between(HoraDePrestado,HoraDevuelto)/24<=2 && (this.genero.equals("CIENTIFICA")) || this.genero.equals("DEPORTIVA") ){
			this.estado = "Perdida";
		}
		else if((int)ChronoUnit.HOURS.between(HoraDePrestado,HoraDevuelto)/24<=3 && (this.genero.equals("GASTRONOMICA"))) {
			this.estado = "Perdida";
		}
		ChronoUnit.HOURS.between(HoraDePrestado,HoraDevuelto);
	}

	public LocalDateTime getHoraDevuelto() {
		return HoraDevuelto;
	}

	public void setHoraDevuelto(LocalDateTime localDateTime) {
		this.HoraDevuelto = localDateTime;
	}
	
}
