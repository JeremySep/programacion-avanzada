import java.util.*;

public class Systema {
	
	private String Nombre;
	private String Accion;
	private String consulta;
	private String Elige;
	private int exit = 0;
	
	Systema(){

		HashSet<String> Estudiantes = new HashSet<String>();
		HashSet<String> Estudiantes2020 = new HashSet<String>();
		Estudiantes.add("Juan");
		Estudiantes.add("Humberto");
		Estudiantes.add("Gonzalo");
		Estudiantes.add("Fidel");
		Estudiantes.add("Augusto");
		Estudiantes.add("Osama");
		Estudiantes.add("Sebastian");
		Estudiantes.add("Michael");
		
		Estudiantes2020.add("Humberto");
		Estudiantes2020.add("Fidel");
		Estudiantes2020.add("Sergei");
		Estudiantes2020.add("Arturo");
		Estudiantes2020.add("Valentino");
		
		Iterator lista = Estudiantes.iterator();
		Iterator lista2 = Estudiantes2020.iterator();
		
		HashSet respaldo = new HashSet();
		HashSet respaldo2 = new HashSet();
		
		System.out.println("Primer conjunto (Estudiantes): ");
		while(lista.hasNext()) {
			System.out.println(lista.next());
		}
		
		System.out.println("Segundo conjunto (Estudiantes2020): ");
		while(lista2.hasNext()) {
			System.out.println(lista2.next());
		}
		
		respaldo.addAll(Estudiantes);
		respaldo2.addAll(Estudiantes2020);
				
		System.out.println("Existe Humberto en el conjunto Estudiantes: " + Estudiantes.contains("Humberto"));
		
		System.out.println("Todos los elementos del conjunto Estudiantes existen en Estudiantes2020: " + Estudiantes2020.containsAll(Estudiantes));
		
		Estudiantes.addAll(Estudiantes2020);
				
		System.out.println("Total de estudiantes registrados: " + Estudiantes);
		
		Estudiantes2020.removeAll(respaldo);
		
		respaldo2.removeAll(Estudiantes2020);
		
		System.out.println("Estudiantes de 2020 que estan el Estudiantes: " + respaldo2);	
		
		
	}	
}
